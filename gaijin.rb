


require 'anemone'
require 'nokogiri'
require 'csv'
 

target_links = []
@link_path = "//body/div[2]/div[5]/div/div/div/div[1]/h3/a"
@httpwww = "https://jobs.gaijinpot.com"

#このリンクをほしい取得したリンクにかえてください。
doc = Nokogiri::HTML(open("https://jobs.gaijinpot.com/job/index/category/13/lang/en"))
doc.xpath(@link_path).each do |link|
  target_links.push( @httpwww + link[:href].to_s)
end


count = 0
Anemone.crawl(target_links) do |anemone|
  anemone.on_every_page do |page|
    doc  = Nokogiri::HTML.parse(page.body)
    tmp_hash = Hash.new{|h,k| h[k]=[]}

    title = doc.xpath("//body/div[2]/div[4]/div[2]/div[1]/div/div[1]/h1").text
    url = target_links[count]
    inc  = doc.xpath("//body/div[2]/div[4]/div[2]/div[1]/div/div[2]/dl/dd[1]").text
    salary = doc.xpath("//body/div[2]/div[4]/div[2]/div[1]/div/div[2]/dl/dd[8]").text
    worktype = doc.xpath("//body/div[2]/div[4]/div[2]/div[1]/div/div[2]/dl/dd[7]").text
    url.match(/\/category\/(.+)\/lang/)
    occupation = $1
    raw_city = doc.xpath("//body/div/div[4]/div[2]/div[1]/div/div[2]/dl/dd[3]").text



    ### Normarize city
    if raw_city.count(",") == 0
      if raw_city == "Tokyo"
        city = ["Other Tokyo"]
      else
        city = ["Other Tokyo"]
      end
    else
      raw_city.match /(.+\,)+\s(.+)\Z/
      raw_city = $1
      prefecture = $2
      if prefecture == "Tokyo"
        ary_city = raw_city.gsub(/\s/, "").split(/\,|\/|　|&|区/)
        city = Array.new
        csv_data = CSV.read('city.csv', headers: false)
        
        ary_city.each do |city_name|
          csv_data.each do |data|
            city << data[1] if data[0] == city_name.downcase
          end
          city << "Other Tokyo" if city.empty?
        end
      else
        city = ["Other Tokyo"]
      end
    end
    
    
    ###normarize occupation
    url.match(/\/category\/(.+)\/lang/)
    occup_num = $1
    csv_data = CSV.read('occupation.csv', headers: false)
    csv_data.each do |data|
      occupation = data[1] if data[0] == occup_num
    end
    
    ###normarize worktype 
    worktype.match /(.+)\s\/.+/
    worktype = $1  
    
    
    puts "----------"
    
    ###push into array
    city.uniq.each do |c|
      tmp_hash[:city] = c
      tmp_hash[:title]   = title
      tmp_hash[:url]     = url
      tmp_hash[:company] = inc.strip.gsub(/\t{2,}/, "\t")
      tmp_hash[:salary]  = salary.strip.gsub(/\t|\n/, "")
      tmp_hash[:occupation] = occupation
      tmp_hash[:worktype]   = worktype.strip.gsub(/\t{2,}/, "\t")

      p tmp_hash
    end
    
    count += 1
  end
end


